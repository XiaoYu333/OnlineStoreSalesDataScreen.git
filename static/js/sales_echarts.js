function showUnreadNews() {
    $(document).ready(function() {
        // sales_manager
        $.ajax({
            url: '/ld_data',
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8',
            data: {},
            success: function (data) {
                console.log(data.sales_manager);
                console.log(data.sales);
                console.log(data.profit);
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('data_manager'));
                var option= {
                    legend: {
                        data: ['销售额', '利润'],
                        textStyle: {color: '#fff'},
                    },
                    radar: {
                        // shape: 'circle',
                        indicator: data.sales_manager
                    },
                    series: [{
                        type: 'radar',
                        data: [
                            {
                                value: data.sales,
                                name: '销售额',
                                itemStyle: {
                                color: '#ff8800',
                                    label: {
                                      show: true, // 显示数值
                                      formatter: function(params) {
                                        return params.value; // 数值格式化
                                      }
                                    }
                                }
                            },
                            {
                                value: data.profit,
                                name: '利润',
                                itemStyle: {
                                color: '#fff200',
                                    label: {
                                      show: true, // 显示数值
                                      formatter: function(params) {
                                        return params.value; // 数值格式化
                                      }
                                    }
                                }
                            }
                        ]
                    }],
                };
                myChart.setOption(option);
            }
        });
        // data_provinve
        $.ajax({
            url: '/map_data',
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8',
            data: {},
            success: function (data) {
                console.log(data.province);
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('data_province'));
                var option = {
                    series: [
                        {
                            name: '销售',
                            type: 'map',
                            geoIndex: 0,
                            data: data.province,
                        },
                    ],
                    tooltip: {
                        formatter: function (params, ticket, callback) {
                        // 获取该省份的销售和利润数据
                        var sale = params.data.sales;
                        var profit = params.data.profit;
                        // 拼接提示框信息
                        return  params.name +'<br />' + '销售额:' + sale + '<br />' +'利润:' + profit;
                        }
                    },
                    visualMap: {
                        min: 1100,
                        max: 273880,
                        left: 'left',
                        top: 'bottom',
                        text: ['高', '低'],//取值范围的文字
                        inRange: {
                            color: ['#fff200', '#ff8800']//取值范围的颜色
                        },
                        show: true,//图注
                        textStyle: {color: '#fff'},
                    },
                    geo: {
                        map: 'china',
                        roam: false,//不开启缩放和平移
                        zoom: 1.23,//视角缩放比例
                        label: {
                            normal: {
                                show: true,
                                fontSize: '15',
                                color: 'rgba(0,0,0,0.7)'
                            }
                        },
                        itemStyle: {
                            normal: {
                                borderColor: 'rgba(0, 0, 0, 0.2)'
                            },
                            emphasis: {
                                areaColor: '#00b2ff',//鼠标选择区域颜色
                                shadowOffsetX: 0,
                                shadowOffsetY: 0,
                                shadowBlur: 20,
                                borderWidth: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    },
                };
                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                // 点击事件
                myChart.on('click', function (params) {
                    var sale = params.data.sales;
                    var profit = params.data.profit;
                    alert(params.name +'  销售额:' + sale + '  利润:' +profit);
                });
            }
        });
        // sales_month
        $.ajax({
            url: '/bar_data',
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8',
            data: {},
            success: function (data) {
                console.log(data.month);
                console.log(data.sales);
                console.log(data.profit);
                // 基于准备好的dom，初始化echarts实例
                var myChart_bar = echarts.init(document.getElementById('data_month'));
                var option_bar = {
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: ['销售额', '利润'],
                        textStyle: {color: '#fff'},
                      },
                    toolbox: {
                        show: true,
                        feature: {
                            magicType: { show: true, type: ['line', 'bar'] },
                        }
                    },
                    calculable: true,
                    xAxis: [
                    {
                        type: 'category',
                        splitLine: {
                          show: false
                        },
                        data: data.month,
                        axisLabel: {
                            show: true,
                            textStyle: {color: '#fff'}
                        },
                        //坐标轴线的颜色以及宽度
                        axisLine: {
                            show: true,
                            lineStyle: {
                                color: "#fff",
                                width: 1,
                                type: "solid"
                            },
                        },
                    }
                    ],
                    yAxis: [
                    {
                        type: 'value',

                        splitLine: {
                          show: false
                        },
                        axisLabel: {
                            show: true,
                            textStyle: {color: '#fff',},
                        },
                        //坐标轴线的颜色以及宽度
                        axisLine: {
                            show: true,
                            lineStyle: {
                                color: "#fff",
                                width: 1,
                                type: "solid"
                            },
                        },
                    }
                    ],
                    series: [
                    {
                        name: '销售额',
                        type: 'bar',
                        data: data.sales,
                        itemStyle: {
                            color: '#ff8800'
                        }
                    },
                    {
                        name: '利润',
                        type: 'bar',
                        data: data.profit,
                        itemStyle: {
                            color: '#fff200'
                        }
                    },
                    ],
                    grid:{ // 让图表占满容器
                        top:"6px",
                        left:"53px",
                        right:"0px",
                        bottom:"33px"
                    },
                };
                myChart_bar.setOption(option_bar);
            }
        });
        // sales_product
        $.ajax({
            url: '/sun_data',
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8',
            data: {},
            success: function (data) {
                console.log(data.detail);
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('data_product'));
                var option = {
                    series: {
                        type: 'sunburst',
                        // emphasis: {
                        //     focus: 'ancestor'
                        // },
                        data: data.detail,
                        radius: [0, '100%'],
                        label: {
                            show: true,
                            formatter: '{b}'
                        },
                        emphasis: {
                            label: {
                                show: true,
                                formatter: '{c}',
                            }
                        },
                        levels: [
                            {
                                itemStyle: {
                                    color: '#00b2ff',
                                }
                            }, {
                                itemStyle: {
                                    color: '#ff8800',
                                }
                            }, {
                                itemStyle: {
                                    color: '#fff200',
                                }
                            }
                        ],
                    },

                };
                myChart.setOption(option);
            }
        });
        // sales_region
        $.ajax({
            url: '/line_data',
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8',
            data: {},
            success: function (data) {
                console.log(data.region);
                console.log(data.sales);
                console.log(data.profit);
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('data_region'));
                var option= {
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: ['销售额','利润'],
                        textStyle: {color: '#fff'}
                    },
                    toolbox: {
                        show: false,
                        feature: {
                            saveAsImage: {
                                show: false,
                            }
                        }
                    },
                    xAxis: {
                        type: 'category',
                        boundaryGap: false,
                        data: data.region,
                        axisLabel: {
                            show: true,
                            textStyle: {color: '#fff'}
                        },
                        //坐标轴线的颜色以及宽度
                        axisLine: {
                            show: true,
                            lineStyle: {
                                color: "#fff",
                                width: 1,
                                type: "solid"
                            },
                        },
                    },
                    yAxis: {
                        type: 'value',
                        axisLabel: {
                            show: true,
                            textStyle: {color: '#fff'}
                        },
                        splitLine: {
                            show: false
                        },
                        //坐标轴线的颜色以及宽度
                        axisLine: {
                            show: true,
                            lineStyle: {
                                color: "#fff",
                                width: 1,
                                type: "solid"
                            },
                        },
                    },
                    series: [
                        {
                            name: '销售额',
                            type: 'line',
                            stack: 'Total',
                            data: data.sales,
                            itemStyle: {
                            color: '#ff8800'
                        }
                        },
                        {
                            name: '利润',
                            type: 'line',
                            stack: 'Total',
                            data: data.profit,
                            itemStyle: {
                            color: '#fff200'
                        }
                        },
                    ],
                    grid:{ // 让图表占满容器
                        top:"6px",
                        left:"65px",
                        right:"11px",
                        bottom:"33px"
                    },
                };
                myChart.setOption(option);
            }
        });
    });
}
setInterval('showUnreadNews()',10000);//轮询执行，20000ms一次
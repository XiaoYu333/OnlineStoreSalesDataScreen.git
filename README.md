# 电商销售可视化大屏

#### 介绍
这是一个兴趣使然开发的一个简单可视化项目，数据来源于某个同学

#### 软件架构
简单的Django+echarts框架实现可视化大屏

#### 安装教程

##### 1.安装 python3.9.1 
##### 2.创建虚拟环境
##### virtualenv创建虚拟环境
1. mkvirtualenv -p python SalesDataScreen_env
2. workon SalesDataScreen_env 
##### conda创建虚拟环境
1. conda create -n SalesDataScreen_env python=3.9.1 -y
2. conda env list
3. conda activate SalesDataScreen_env 

#### 安装依赖
pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple some-package

#### 使用说明

1. 运行manage.py启动项目 
2. 然后就可以看图了

#### 参与贡献

1.  Fork 本仓库
2.  新建 master 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

"""OnlineStoreSalesDataScreen URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from OnlineStoreSalesDataScreen import views

urlpatterns = [
    path("admin/", admin.site.urls),
    path("",views.index),
    path("bar_data",views.bar_data),
    path("map_data",views.map_data),
    path("sun_data",views.sun_data),
    path("line_data",views.line_data),
    path("ld_data",views.ld_data),
]